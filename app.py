#!/usr/bin/env python

from flask import Flask
from flask import render_template
from flask.json import jsonify

from geoip import geolite2
import requests

import socket


class FlaskApp(Flask):
    jinja_options = Flask.jinja_options.copy()
    jinja_options.update(dict(
        block_start_string='<%', block_end_string='%>',
        variable_start_string='%%', variable_end_string='%%',
        comment_start_string='<#', comment_end_string='#>',
    ))


app = FlaskApp(__name__)
app.config['DEBUG'] = True


@app.route("/")
def index():
    return render_template('index.html')

def get_ip_from_domain(domain):
    return socket.gethostbyname(domain)

@app.route("/get-data/<string:query>")
def get_data(query):
    try:
        d = geolite2.lookup(get_ip_from_domain(query)).to_dict()
        result = {k: d[k] for k in d if k != 'subdivisions'}
        result['query'] = query
        result['status'] = ''
        if any([x == 'None' for x in result.values()]):
            result['status'] = 'warning'
        print result.values()
        return jsonify(result)
    except:
        result = {"query": query, "ip": "ERROR", "status": "danger"}
        return jsonify(result)

@app.route("/get-http-headers/<string:query>")
def get_http_headers(query):
    if not query.startswith('http://'):
        query = 'http://' + query
    r = requests.get(query)
    return jsonify(r.headers)


if __name__ == "__main__":
    app.run(None, 8000)
